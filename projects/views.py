from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def show_projects(request):
    projects_list = Project.objects.filter(owner=request.user)
    context = {"projects_list": projects_list}
    return render(request, "projects/list.html", context)


@login_required
def show_details(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"project": project}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            model_instance = form.save(commit=False)
            model_instance.owner = request.user
            model_instance.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {"form": form}

    return render(request, "projects/create.html", context)
