from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task

# Create your views here.


@login_required
def create_tasks(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            model_instance.owner = request.user
            model_instance.save()
            return redirect("list_projects")
    else:
        form = TaskForm()

    context = {"form": form}

    return render(request, "tasks/create.html", context)


@login_required
def show_tasks(request):
    tasks_list = Task.objects.filter(assignee=request.user)
    context = {"tasks_list": tasks_list}
    return render(request, "tasks/list.html", context)
