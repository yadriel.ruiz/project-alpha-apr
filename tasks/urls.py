from django.urls import path
from tasks.views import create_tasks, show_tasks

urlpatterns = [
    path("mine/", show_tasks, name="show_my_tasks"),
    path("create/", create_tasks, name="create_task"),
]
